import React from 'react';
import {Button, Text, Platform, ScrollView, StyleSheet} from 'react-native';
import {DrawerNavigator} from 'react-navigation';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob'

import calculator from './screens/calculator';
import proteinFoods from './screens/proteinFoods';
import calorieFoods from './screens/calorieFoods';


const Drawer = DrawerNavigator(
    {
        "Diet calculator": {
            
            screen: calculator,
        },
        "Cheap protein foods": {
            
            screen: proteinFoods,
        },
        "High calorie foods": {
           
            screen: calorieFoods,
        },
    },
    {
        initialRouteName: 'Diet calculator',
        drawerPosition: 'left',
        contentOptions: {
            activeTintColor: 'red',
        }
    }
);

export default Drawer;