import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DrawerActions } from 'react-navigation';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob'

export default class proteinFoods extends React.Component {
    static navigationOptions = {
        tabBarlabel: 'Screen 2',
        drawerIcon: ({tintColor}) =>{
            return (
                <MaterialIcons
                name="money-off"
                size={24}
                style={{color: tintColor}}
                >

                </MaterialIcons>

            );
        }
    }

    render(){
        return <View
        style={styles.container}
        >
            <View style={{flexDirection:'row', height: '8%', backgroundColor: '#E04E41',}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontSize:20, marginLeft: '15%', color:'#FAFDF6' }}>Cheap protein foods</Text>
           
            </View>
        
            <ScrollView>
            
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/brocli.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/chiken.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/chikpeas.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/egg.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/milk.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/oats.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/peanut.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/shrimp.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/soybeans.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/proteinFood/telapia.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>

            </ScrollView>
            
            <AdMobBanner
          adSize="smartBannerLandscape"
          adUnitID="ca-app-pub-4727449928714385/3010501178"
         />

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    
  });