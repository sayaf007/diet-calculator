import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
  } from 'react-native-admob'

export default class calorieFoods extends React.Component {
    static navigationOptions = {
        tabBarlabel: 'Screen 3',
        drawerIcon: ({tintColor}) =>{
            return (
                <Entypo
                name="bar-graph"
                size={24}
                style={{color: tintColor}}
                >

                </Entypo>
            );
        }
    }

    render(){
        return <View
        style={styles.container}
        >
            <View style={{flexDirection:'row', height: '8%', backgroundColor: '#E04E41',}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontSize:20, marginLeft: '15%', color:'#FAFDF6' }}>High calorie foods</Text>
           
            </View>
        
            <ScrollView>
            
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/almond.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/avocado.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/beef.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/chiken.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/chikpeas.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/coconut.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/dates.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/noodles.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/oats.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/peanut.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/rice.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>
            <View style={{padding:5}}>
            <Image
                source={require('../Image/calorieFood/soybeans.jpg')}
                style={{
                height: 200,
                width: '100%',
                }}
                resizeMode="stretch"
                />
            </View>

            
                
            </ScrollView>
        <AdMobBanner
          adSize="smartBannerLandscape"
          adUnitID="ca-app-pub-4727449928714385/3010501178"
         />

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    
  });