import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ImageBackground,
  KeyboardAvoidingView
} from "react-native";
import SearchInput, { createFilter } from "react-native-search-filter";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/FontAwesome";
import Swipeout from "react-native-swipeout";
import _ from "lodash";
import Data from "../dataBangla";
import { DrawerActions } from "react-navigation";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob'

const KEYS_TO_FILTERS = ["food"];

Array.prototype.sum = function(prop) {
  var total = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total += this[i][prop];
  }
  return total;
};
Array.prototype.sum1 = function(prop) {
  var total1 = 0;
  for (var i = 0, _len = this.length; i < _len; i++) {
    total1 += this[i][prop];
  }
  return total1;
};

/*
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
*/

console.disableYellowBox = true;

export default class calculator extends Component {
  static navigationOptions = {
    tabBarlabel: "Screen 1",
    drawerIcon: ({ tintColor }) => {
      return (
        <FontAwesome
          name="balance-scale"
          size={20}
          style={{ color: tintColor }}
        />
      );
    }
  };

  icon = <Icon name="sort-down" size={15} color="black" />;
  constructor(props) {
    super(props);
    this.state = {
      _selectedFood: [],
      searchTerm: "",
      searchFood: false,
      totalCalorie: 0,
      totalProtein: 0,
      manageUnit: false,
      newUnit: 1,
      selectedIndx: "",
      selectedCalorie: "",
      selectedProtein: "",
      deleteIndx: "",
      onpress: true
    };
  }

  selectedItem = (name, unit, newUnit, calorie, protein, i) => {
    const result = this.state._selectedFood.find(items => items.index === i);
    if (result) {
      alert("Item already added");
    } else {
      console.log(name, unit, newUnit, calorie, protein, i);
      this.setState({ searchFood: false });
      this.state._selectedFood.push({
        food: name,
        unit: unit,
        newUnit: newUnit,
        calorie: calorie,
        protein: protein,
        index: i
      });
      this.setState({ deleteIndx: 0 });
      console.log(this.state._selectedFood);
    }
  };

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  searchFoods = () => {
    const filteredFood = Data.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    return (
      <Modal
        isVisible={this.state.searchFood}
        backdropColor={"transparent"}
        backdropOpacity={1}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}
        onBackdropPress={() => this.setState({ searchFood: false })}
        onRequestClose={() => this.setState({ searchFood: false })}
      >
        <View
          style={{
            backgroundColor: "#F2F2F2",
            width: "70%",
            marginLeft: "15%",
            borderWidth: 1,
            borderRadius: 10
          }}
        >
          <SearchInput
            onChangeText={term => {
              this.searchUpdated(term);
            }}
            //   inputViewStyles={styles.searchInput}
            placeholder={"Type a food name to search"}
            fuzzy={true}
            inputFocus={true}
            style={styles.searchInput}
          />
          <ScrollView style={{ height: "50%" }}>
            {filteredFood.map((item, i) => {
              return (
                <View>
                  <TouchableOpacity
                    key={i}
                    onPress={() =>
                      this.selectedItem(
                        item.food,
                        item.unit,
                        item.newUnit,
                        item.calories,
                        item.protein,
                        i
                      )
                    }
                  >
                    <View>
                      <Text
                        style={{
                          textAlign: "center",
                          borderBottomWidth: 1,
                          fontSize: 18
                        }}
                      >
                        {item.food}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </Modal>
    );
  };

  manageUnit = () => {
    return (
      <Modal
        isVisible={this.state.manageUnit}
        backdropColor={"transparent"}
        backdropOpacity={1}
        animationIn="zoomInDown"
        animationOut="zoomOutUp"
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}
        onBackdropPress={() => this.setState({ manageUnit: false })}
        onRequestClose={() => this.setState({ manageUnit: false })}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#F2F2F2",
            width: "40%",
            height: "30%",
            marginLeft: "30%",
            borderWidth: 1,
            borderRadius: 10
          }}
        >
          <TextInput
            placeholder={"Add unit"}
            onChangeText={text => {
              this.validateNumber(text);
            }}
            style={{
              borderWidth: 1,
              textAlign: "center",
              padding: 5,
              backgroundColor: "#dbdbdb"
            }}
          />

          <TouchableOpacity
            style={{ padding: 5, width: "70%" }}
            onPress={() => {
              this.unitUpdated();
            }}
          >
            <Text
              style={{
                textAlign: "center",
                padding: 5,
                backgroundColor: "#6CB44E",
                color: "white"
              }}
            >
              Update
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  validateNumber = text => {
    //For only positive
    //var regexp = /^\d+\.\d{0,8}$/;
    //For both positive & negative
    
    var regexp = /^[-+]?\d*\.?\d*(\d+[eE][-+]?)?\d+$/;
    // ['1.2','-1.2', '1.12345678', '-1.12345678', '1.123456789', '-1.23456789', ',,,', 'abcd', '1..2', '1.2.3', '1.2.3-'].forEach(element => {
    //     console.log('Element ', element, ' Rex ', regexp.test(element));
    // });
    if (regexp.test(text)) {
      //if (/^\d+(?:\.\d+)?$/gm.test(text)) {
      //if (/^(-)?\d+\.\d{0,8}$/.test(text)) {
      this.setState({newUnit:text});
    } else {
      alert('Enter number value');
    }
  };

  unitUpdated() {
    this.state._selectedFood.find(
      v => v.index == this.state.selectedIndx
    ).calorie = parseFloat(this.state.selectedCalorie) * this.state.newUnit;
    this.state._selectedFood.find(
      v => v.index == this.state.selectedIndx
    ).protein = parseFloat(this.state.selectedProtein) * this.state.newUnit;
    this.state._selectedFood.find(
      v => v.index == this.state.selectedIndx
    ).newUnit = this.state.newUnit;
    this.setState({ manageUnit: false });
  }

  deleteItm = itm => {
    return [
      {
        text: "delete",
        onPress: () => {
          this.state._selectedFood = this.state._selectedFood.filter(function(
            returnableObjects
          ) {
            return returnableObjects.index !== itm;
          });

          this.setState({ onpress: !this.state.onpress });
          this.GenericRow();
          console.log(this.state._selectedFood);
        }
        //
      }
    ];
  };

  GenericRow() {
    return this.state._selectedFood.map((item, i) => {
      return (
        <Swipeout right={this.deleteItm(item.index)}>
          <View key={i} style={{ flexDirection: "row" }}>
            <Text style={styles.listItem}>
              {item.food}
              {console.log(item.food)}
            </Text>
            <Text
              onPress={() => {
                this.setState({ manageUnit: true });
                if (item.newUnit === "1") {
                  this.setState({
                    selectedIndx: item.index,
                    selectedCalorie: item.calorie,
                    selectedProtein: item.protein
                  });
                }
              }}
              style={styles.listItem}
            >
              ({item.unit}) x {item.newUnit} {this.icon}
            </Text>
            <Text style={styles.listItem}>
              {(item.calorie = parseFloat(item.calorie)).toFixed(2)}
            </Text>
            <Text style={styles.listItem}>
              {(item.protein = parseFloat(item.protein)).toFixed(2) + "g"}
            </Text>
          </View>
        </Swipeout>
      );
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            height: "8%",
            backgroundColor: "#E14F42"
          }}
        >
          <MaterialIcons
            name="list"
            size={40}
            style={{ paddingLeft: 5 }}
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.openDrawer())
            }
          />

          <Text
            style={{
              alignSelf: "center",
              fontSize: 20,
              marginLeft: "20%",
              color: "#FAFDF6",
              fontFamily: "Roboto"
            }}
          >
            Diet calculator
          </Text>
        </View>
        <ImageBackground
          style={styles.container}
          source={require("../Image/backgroundImage.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.2 }}
        >
        
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              padding: 5
            }}
          >
            <View style={{ padding: 10 }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ searchFood: true });
                }}
                style={{ padding: 15, backgroundColor: "#E04E41" }}
              >
                <Text
                  style={{
                    fontSize: 20,
                    color: "#FAFDF6"
                  }}
                >
                  Add food
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{ padding: 15, backgroundColor: "#6CB44E" }}
              onPress={() => {
                this.setState({ _selectedFood: [] });
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  color: "#FAFDF6"
                }}
              >
                Clear list
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              padding: 10,
              height: "60%",
              borderWidth: 1,
              width: "98%",
              marginLeft: "1%",
              borderRadius: 5,
              backgroundColor: "transeparent"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "#E04E41",
                borderWidth: 1
              }}
            >
              <Text
                style={{
                  width: "25%",
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  borderRightWidth: 1,
                  fontSize: 18
                }}
              >
                Name
              </Text>
              <Text
                style={{
                  width: "25%",
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  borderRightWidth: 1,
                  fontSize: 18
                }}
              >
                Unit
              </Text>
              <Text
                style={{
                  width: "25%",
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  borderRightWidth: 1,
                  fontSize: 18
                }}
              >
                Calorie
              </Text>
              <Text
                style={{
                  width: "25%",
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  fontSize: 18
                }}
              >
                Protein
              </Text>
            </View>

            <ScrollView>{this.GenericRow()}</ScrollView>
          </View>

          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <View style={styles.total}>
              <Text
                style={{
                  textAlign: "center",
                  backgroundColor: "#E04E41",
                  padding: 5,
                  fontSize: 16,
                  color: "#FBFDF8"
                }}
              >
                Total Calories :{" "}
                {parseFloat(this.state._selectedFood.sum1("calorie")).toFixed(
                  2
                )}
              </Text>
            </View>

            <View style={styles.total}>
              <Text
                style={{
                  textAlign: "center",
                  backgroundColor: "#E04E41",
                  padding: 5,
                  fontSize: 16,
                  color: "#FBFDF8"
                }}
              >
                Total Protein :{" "}
                {parseFloat(this.state._selectedFood.sum("protein")).toFixed(2)}
                {"g"}
              </Text>
              {console.log(_.sumBy(this.state._selectedFood, "protein"))}
              {/* {console.log(parseFloat(this.state._selectedFood.sum("protein")),parseFloat(this.state._selectedFood.sum("calorie")))} */}
            </View>
          </View>
          <AdMobBanner
          adSize="smartBannerLandscape"
          adUnitID="ca-app-pub-4727449928714385/3010501178"
         />
          {this.searchFoods()}
          {this.manageUnit()}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2"
  },
  searchInput: {
    textAlign: "center",
    borderRadius: 15,
    width: "94%",
    marginLeft: "2%",
    marginTop: "2%",
    backgroundColor: "#dbdbdb"
  },
  head: { height: 40, backgroundColor: "#f1f8ff" },
  text: { margin: 6 },

  total: {
    padding: 5,
    width: "40%"
  },
  listItem: {
    width: "25%",
    textAlign: "center",
    backgroundColor: "#E04E41",
    overflow: "hidden",
    borderWidth: 1,
    color: "#fff",
    fontSize: 15
  }
});
